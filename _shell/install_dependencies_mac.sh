#!/bin/bash

# Show usage
show_help() {
  cat <<-EOF
  Usage: ${0##*/} [-hv] [-force-update] [-force-system]
  Installs Homebrew, Homebrew Cask, Vagrant, Virtualbox and POW
      -h              display this help and exit
      -v              verbose mode. Can be used multiple times for increased verbosity.
      -force-update   Force update if any of the tools is already installed.
      -force-system   Force execution on others systems (not only Macs).
	EOF
}

# Setting default variables
verbose=0
force_update=false

# Parsing arguments
while :
do
  case $1 in
    -h | --help | -\?)
      show_help
      exit 0
      ;;
    --force-update)
      force_update=true
      shift
      ;;
    -v | --verbose)
      verbose=$((verbose+1))
      shift
      ;;
    --)
      shift
      break
      ;;
    -*)
      printf >&2 'WARN: Unknown option (ignored): %s\n' "$1"
      shift
      ;;
    *)
      break
      ;;
  esac
done

# Setting default variables
if [ "$(uname)" != "Darwin" ]; then
  cat <<-EOF
  This script was meant to be executed on a Mac
	EOF
  exit 0
fi

# Exits if vagrant is installed
if hash vagrant 2>/dev/null && [ "$force_update" = "false" ]; then
  cat <<-EOF
  Vagrant is already installed in your system.
  If you want to update runs '$0 --force-update'
	EOF
  [ $verbose -gt 0 ] && echo "  Vagrant version: `vagrant --version`"
  exit 0
fi

# Install / Update Homebrew
if hash brew &>/dev/null; then
  echo "  Homebrew is already installed."
  [ $verbose -gt 0 ] && echo "  Homebrew current version: `brew --version`"

  if [ "$force_update" = "true" ]; then
    [ $verbose -gt 0 ] && echo "  Updating Homebrew..."
    brew update
    [ $verbose -gt 0 ] && echo "  Homebrew new version: `brew --version`"
  fi
else
  echo "  Homebrew is not installed. Lets install it."
  ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
  [ $verbose -gt 0 ] && echo "  Homebrew installed version: `brew --version`"
fi

# Install / Update Homebrew-Cask
if brew ls brew-cask &>/dev/null; then
  echo "  Homebrew-Cask is already installed."

  [ $verbose -gt 0 ] && echo "  Homebrew-Cask current version: `brew cask --version`"

  if [ "$force_update" = "true" ]; then
    [ $verbose -gt 0 ] && echo "  Updating Homebrew-Cask"
    brew upgrade brew-cask
    [ $verbose -gt 0 ] && echo "  Homebrew-Cask new version: `brew cask --version`"
  fi
else
  echo "  Homebrew-Cask is not installed. Lets install it."
  brew tap phinze/homebrew-cask && brew install brew-cask
  [ $verbose -gt 0 ] && echo "  Homebrew-Cask installed version: `brew cask --version`"
fi

# Install Virtualbox
hash vboxmanage 2>/dev/null && [ $verbose -gt 0 ] && echo "  Virtualbox current version: `vboxmanage --version`"
brew cask install virtualbox
[ $verbose -gt 0 ] && echo "  Virtualbox new version: `vboxmanage --version`"

# Install Vagrant
hash vagrant 2>/dev/null && [ $verbose -gt 0 ] && echo "  Vagrant current version: `vagrant --version`"
brew cask install vagrant
[ $verbose -gt 0 ] && echo "  Vagrant new version: `vagrant --version`"

# Install Pow.cx
[ $verbose -gt 0 ] && echo "  Installing POW"
curl get.pow.cx | sh
