#!/bin/bash

# Show usage
show_help() {
  cat <<-EOF
  Usage: ${0##*/} [-h]
  Install Dependencies, Create and Provision VirtualMachine, Create hosts entry
      -h              display this help and exit
	EOF
}

# Setting default variables
platform="$(uname)"

# Parsing arguments
while :
do
  case $1 in
    -h | --help | -\?)
      show_help
      exit 0
      ;;
    --)
      shift
      break
      ;;
    -*)
      printf >&2 'WARN: Unknown option (ignored): %s\n' "$1"
      shift
      ;;
    *)
      break
      ;;
  esac
done

# Check if Vagrant is installed
if ! hash vagrant 2>/dev/null; then
  if [ "$platform" != "Darwin" ]; then
    cat <<-EOF
    You must install Virtualbox and Vagrant before execute this script
		EOF
    exit 1
  else
    # If it is a Mac, install it
    ./install_dependencies.sh
  fi
fi

# Starts Provision
vagrant up

# Create domain
if [ -d "$HOME/.pow" ]; then
  echo "8080" > $HOME/.pow/agilebrazil2014
else
  echo -e "\n127.0.0.1 agilebrazil2014.dev" | sudo tee -a /etc/hosts >/dev/null
fi
