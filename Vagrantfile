# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "wordpress-bootstrap"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  # Set share folder permissions to 777 so that apache can write files
  config.vm.synced_folder ".", "/vagrant", :nfs => { mount_options: ['dmode=777','fmode=666'] }
  config.vm.network :private_network, ip: "33.33.33.10"

  config.ssh.forward_agent = true

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "600"]

    vb.customize [
      "setextradata", :id,
      "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"
    ]

    vb.customize [
      "setextradata", :id,
      "VBoxInternal/Devices/ahci/0/LUN#[0]/Config/IgnoreFlush", "1"
    ]
  end

  # working around to get private repos (using SSH Fowarding)
  config.vm.provision :shell do |shell|
    shell.inline = "touch $1 && chmod 0440 $1 && echo $2 > $1"
    shell.args = %q{/etc/sudoers.d/root_ssh_agent "Defaults    env_keep += \"SSH_AUTH_SOCK\""}
  end

  # working around to get private repos (using SSH Fowarding)
  config.vm.provision :shell do |shell|
    shell.inline = "mkdir -p $1 && touch $2 && ssh-keyscan -H $3 >> $2 && chmod 600 $2"
    shell.args = %q{/root/.ssh /root/.ssh/known_hosts "github.com"}
  end

  # Enable provisioning with chef solo, specifying a cookbooks path, roles
  # path, and data_bags path (all relative to this Vagrantfile), and adding
  # some recipes and/or roles.
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = "cookbooks"
    chef.data_bags_path = "data_bags"

    # List of recipes to run
    chef.add_recipe "vagrant_main"
    chef.add_recipe "vagrant_main::nodejs"
    chef.add_recipe "vagrant_main::wordpress"
    chef.add_recipe "vagrant_main::sites"
    # chef.log_level = :debug
  end

  forward_port = ->(guest, host = guest) do
    config.vm.network :forwarded_port,
      guest: guest,
      host: host,
      auto_correct: true
  end

  forward_port[80, 8080]       # nginx/apache
  forward_port[3306, 3307]     # mysql
  forward_port[1080]           # mailcatcher

  # Other foward ports that should be usable
  # forward_port[3000]           # rails
  # forward_port[9292]           # rack
  # forward_port[4567]           # sinatra
  # forward_port[8888]           # jasmine
  # forward_port[1234]           # node
  # forward_port[5432]           # postgresql
  # forward_port[6379]           # redis
  # forward_port[9200]           # elasticsearch
  # forward_port[27017, 27018]   # mongodb
end
