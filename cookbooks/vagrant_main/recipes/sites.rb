# Initialize sites data bag
sites = []
begin
  sites = data_bag("sites")
rescue
  puts "Unable to load sites data bag."
end

# Configure sites
sites.each do |name|
  site = data_bag_item("sites", name)

  if site["git_repo_url"]
    git "/vagrant/public/#{site["host"]}" do
      repository site["git_repo_url"]
      reference "master"
      action :sync
    end
  end

  # Add site to apache config
  web_app site["host"] do
    template "sites.conf.erb"
    server_name site["host"]
    server_aliases site["aliases"]
    server_include site["include"]
    docroot site["docroot"]?site["docroot"]:"/vagrant/public/#{site["host"]}"
  end

  # Add site info in /etc/hosts
  bash "hosts" do
    code "echo 127.0.0.1 #{site["host"]} #{site["aliases"].join(' ')} >> /etc/hosts"
    not_if "grep -v '^#.*' /etc/hosts | grep -eq '\\b#{site["host"]}\\b'" #
  end

  # Create database and user
  if site["database"]
    execute "create_db_#{site["database"]["name"]}" do
      command "mysql -u root --password=vagrant -Bse 'CREATE DATABASE #{site["database"]["name"]}; GRANT ALL ON #{site["database"]["name"]}.* to #{site["database"]["user"]}@#{site["database"]["host"]} identified by \"#{site["database"]["pass"]}\";'"
      not_if "mysql -u root --password=vagrant -Bse 'Show Databases;' | grep '#{site["database"]["name"]}'"
    end
  end

  # Create WordPress App
  if site["wordpress"]
    # Create wp-config.php file
    execute "create_wp-config_#{site["host"]}" do
      cwd "/vagrant/public/#{site["host"]}"
      user "vagrant"
      command "wp core config --dbname=#{site["database"]["name"]} --dbuser=#{site["database"]["user"]} --dbprefix=#{site["wordpress"]["dbprefix"]} --dbpass=#{site["database"]["pass"]}"
      not_if { File.exists?("/vagrant/public/#{site["host"]}/wp-config.php") }
    end

    # Import database
    execute "import_db_#{site["host"]}" do
      cwd "/vagrant/public/#{site["host"]}"
      user "vagrant"
      command "wp db import /vagrant/dumps/#{site["wordpress"]["file_to_import"]}"
      not_if "mysql -u root --password=vagrant -Bse \"Use '#{site["database"]["name"]}'; Show Tables;\" | grep #{site["wordpress"]["dbprefix"]}"
    end

    # Change admin password
    execute "change_admin_pass_#{site["host"]}" do
      cwd "/vagrant/public/#{site["host"]}"
      user "vagrant"
      command "wp user update admin --user_pass=vagrant"
    end

    # Install Plugins
    # wp plugin install all-in-one-wp-migration
    site["wordpress"]["plugins_to_install"].each do |plugin|
      execute "install_plugin_#{plugin}_on_#{site["host"]}" do
        cwd "/vagrant/public/#{site["host"]}"
        user "vagrant"
        command "wp plugin install #{plugin}"
        not_if "wp plugin is-installed #{plugin}" #
      end
    end
  end
end

# Disable default site
apache_site "default" do
  enable false
end
