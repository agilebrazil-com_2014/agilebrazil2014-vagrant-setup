maintainer       "MustacheLabs"
maintainer_email "willian.arantes@mustachelabs.cc"
license          "All rights reserved"
description      "Installs/Configures a Bootstrap WordPerss environment. Strongly based on http://github.com/r8/vagrant-lamp/"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.5"
